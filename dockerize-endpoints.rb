#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
# author:  	maxime déraspe
# email:	maxime@deraspe.net
# date:    	2015-06-19
# version: 	0.01

require 'shell-spinner'
require 'docker'
require 'yaml'
require 'pp'


class EndpointDockerizer

  DOCKER_IMG = 'zorino/docker-blazegraph'
  FILE_FMT = ["nq", "nq.gz",
              "nt", "nt.gz",
              "n3", "n3.gz",
              "ttl","ttl.gz",
              "owl", "owl.gz",
              "xml", "xml.gz"]

  # initialize class
  def initialize config, volume, options

    @host = check_host_specs
    @config = config
    @volume = volume
    @endpoints = load_yaml @config
    @options = options

    check_config

    # image = Docker::Image.create('fromImage' => 'zorino/centos-blazegraph')
    ShellSpinner "Loading #{DOCKER_IMG} docker image" do
      image = Docker::Image.create('fromImage' => DOCKER_IMG)
    end

  end


  # checkup host specs
  def check_host_specs

    host_info = {}

    cpuinfo = `cat /proc/cpuinfo`

    cpuspec = {}
    cpuspec_gathered = false
    nb_core = 0

    cpuinfo.split("\n").each do |l|

      if l =~ /^processor/
        nb_core += 1
      end

      if cpuspec_gathered
        next
      else
        if l =~ /^model name/
          host_info[:cpu_model] = l.split(":")[1].strip
        elsif l =~ /^cpu MHz/
          host_info[:cpu_speed] = l.split(":")[1].strip + " MHz"
        elsif l =~ /^cache size/
          host_info[:cpu_cache] = l.split(":")[1].strip
          bits = `getconf LONG_BIT`
          host_info[:architecture] = bits.to_i
          cpuspec_gathered = true
        end
      end

    end
    host_info[:cpu_number] = nb_core

    meminfo = `cat /proc/meminfo`
    meminfo.split("\n").each do |l|
      if l.include? "MemTotal:"
        total_mem = l.split("MemTotal:")[1].gsub("kB","").strip
        host_info[:memory] = total_mem.to_f
      end
    end

    puts "# Host Info :"
    pp host_info
    puts ""

    host_info

  end


  # checkup for config file
  def check_config

    need_new_config = false

    if ! @endpoints[0].has_key? :port
      add_port_to_db
      need_new_config = true
    end

    if ! @endpoints[0].has_key? :raw_files
      volume_files_checkup
      need_new_config = true
    end

    if need_new_config
      save_current_config
    end

  end

  # add port to @endpoints.. to be mapped on host, range = [9001 += nb_of_endpoints]
  def add_port_to_db

    port_iter = 9001

    @endpoints.each do |edp|
      edp[:port] = port_iter
      port_iter += 1
    end

  end


  # add raw_files array to @endpoints
  def single_volume_files_checkup edp

    if ! Dir.exist? "#{@volume}/#{edp[:database]}/raw_data/zz_loaded"
      Dir.mkdir("#{@volume}/#{edp[:database]}/raw_data/zz_loaded")
    end
    files = Dir.glob("#{@volume}/#{edp[:database]}/raw_data/*.{#{FILE_FMT.join(',')}}")
    edp[:raw_files] = []
    files.each do |f|
      edp[:raw_files] << f.gsub("#{@volume}/#{edp[:database]}/raw_data/", "/mnt/graphs/raw_data/")
    end

  end


  # add raw_files array to @endpoints for all endpoints
  def volume_files_checkup

    @endpoints.each do |edp|
      single_volume_files_checkup edp
    end

  end


  # start a loader only container
  def start_loader_container endpoint

    # Remove 000_loader container before starting a new one
    begin
      existing_container = Docker::Container.get("000_loader")
      puts "Deleting current 000_loader container"
      existing_container.stop
      existing_container.delete(:force => true)
    rescue
    end

    memory = ((@host[:memory]*@options[:ram_ratio])/(1024*1024)).floor

    current_dir = ""
    if @volume[0] != "/"
      current_dir = Dir.pwd
    end

    container = Docker::Container.create('Image' => DOCKER_IMG,
                                         'Volumes':
                                           {
                                             '/mnt/graphs' => {}
                                           },
                                         'Entrypoint' => 'start-blazegraph.sh',
                                         'Cmd' => ["#{memory}g","1"],
                                         "ExposedPorts": {
                                                           "9999/tcp": {}
                                                         },
                                         'HostConfig': {'PortBindings':
                                                          {
                                                            '9999/tcp':
                                                             [{ "HostPort": "9999" }]
                                                          },
                                                        'Binds': ["#{current_dir}/#{@volume}/#{endpoint[:database]}:/mnt/graphs"],
                                                        'RestartPolicy': {'MaximumRetryCount': 10, 'Name': 'on-failure'},
                                                       }
                                        )

    # puts container.start
    container.start
    container.rename("000_loader")

    sleep 5                    # breathe 5 seconds to let blazegraph (java) boot
    return container.info['id']

  end


  # will load the raw RDF files into the graph endpoints
  def load_RDF_into_blazegraph endpoint

    container_id = start_loader_container endpoint
    container = Docker::Container.get(container_id)

    puts "loader-container :" + container.info['id']
    pp container.info['State']

    puts "\nLoading graph into #{endpoint[:database]} blazegraph.."

    sleep 5

    endpoint[:raw_files].each do |f|

      number_of_try = 5

      begin

        ShellSpinner "-file: #{f}" do

          # puts container.exec(["load-graph.sh", "'update=LOAD <file://#{f}>;'"])

          out = `docker exec #{container.info['id']} load-graph.sh 'update=LOAD <file://#{f}>;' 2>&1`
          # puts out

          status = []
          out.split("\n").each do |l|
            if l.include? "COMMIT:"
              l.split(", ").each_with_index do |e,i|
                x = e.gsub(/.*=/,"").gsub('</p',"")
                if i == 0
                  status << "TimeElapsed: " + x.to_s
                elsif i == 1
                  status << "CommitTime: " + x.to_s
                elsif i == 2
                  status << "NewTriples: " + x.to_s
                end
              end
            end
          end

          # puts "**Summary:"
          if status.empty?
            # puts "!!Upload didn't work.."
            raise "#{out}"
          else
            # Upload did WORK
            puts "**Summary:"
            puts status.join("\n")
            container.exec(["mv", "#{f}", "/mnt/graphs/raw_data/zz_loaded/"])
          end

          # FIXME Content-Type Problem
          # when calling the exec with container Bad Content-Type ..

          # puts "/opt/blazegraph/utils/Load-Graph.sh \'update=LOAD <file://#{f}>;\'"
          # command = ["/opt/blazegraph/utils/Load-Graph.sh", "\'update=LOAD <file://#{f}>;\'"]
          # container.exec(command)
          # puts container.exec(["/opt/blazegraph/utils/Load-Graph.sh", "'update=LOAD <file://#{f}>'"])

          # end

        end

      rescue

        if number_of_try > 0
          sleep (10*(6-number_of_try))
          number_of_try -= 1
          retry
        else
          date = Time.new.to_s.split(" ")[0]
          File.open("boostrap.log.#{date}","a") do |fout|
            fout.write("upload_fail\t#{endpoint[:database]}\t#{f}\n")
          end
        end

      end

    end

    container.stop
    container.delete
    sleep 5                   # let it unmount the volume before starting the real endpoint container

  end

  # will load the raw RDF files for all the endpoints
  def load_RDF_into_blazegraph_all

    @endpoints.each do |edp|
      load_RDF_into_blazegraph edp
    end

  end

  # load yaml config and return endpoints hash
  def load_yaml yaml_file
    hash = YAML::load_file(yaml_file)
  end

  # stop a container
  def stop_container edp

    ShellSpinner "Stopping #{edp[:database]} container" do
        begin
          existing_container = Docker::Container.get("#{edp[:database]}")
          existing_container.stop
        rescue
          puts "No container exist for this database !"
        end
    end

  end

  # start a container
  def start_container endpoint, memory

    ShellSpinner "Launching #{endpoint[:database]} container on port #{endpoint[:port]}" do


      # deal with memory option
      memory[-1].downcase!
      if memory[-1] != "g" and memory[-1] != "m" and memory[-1] != "%"
        puts "bad memory format #{memory} ! 1024m 4g or 25%"
        break
      end
      if memory[-1] == "%"
        memory = ((@host[:memory]*memory[0..-1]/100)/(1024*1024)).floor
        memory = memory.to_s + "g"
      end

      # containers
      all_containers = Docker::Container.all(:all => true)
      container_exist = false
      existing_container = nil

      all_containers.each do |c|
        if c.info["Names"][0].gsub("/","") == endpoint[:database]
          container_exist = true
          existing_container = Docker::Container.get("#{endpoint[:database]}")
          break
        end
      end

      if container_exist == true

        puts "#Container for #{endpoint[:database]} already exist with ID:#{existing_container.info['id'][0..12]}"
        puts "#State : "
        pp(existing_container.info['State'])
        if existing_container.info['State']['Running'] == false
          ShellSpinner "## Restarting #{endpoint[:database]} container on port #{endpoint[:port]}" do
            existing_container.start
          end
        end
        endpoint[:docker_id] = existing_container.info['id']

      else

        current_dir = ""
        if @volume[0] != "/"
          current_dir = Dir.pwd
        end

        # Container doesn't existe create it
        ShellSpinner "#Creating NEW container for #{endpoint[:database]}" do

          # Container doesn't existe create it
          container = Docker::Container.create('Image' => DOCKER_IMG,
                                               'Volumes':
                                                 {
                                                   '/mnt/graphs' => {}
                                                 },
                                               'Entrypoint' => 'start-blazegraph.sh',
                                               'Cmd' => ["#{memory}","0"],
                                               "ExposedPorts": {
                                                                 "9999/tcp": {}
                                                               },
                                               'HostConfig': {'PortBindings':
                                                                {
                                                                  '9999/tcp':
                                                                   [{ "HostPort": "#{endpoint[:port]}" }]
                                                                },
                                                              'Binds': ["#{current_dir}/#{@volume}/#{endpoint[:database]}:/mnt/graphs"],
                                                              'RestartPolicy': {'MaximumRetryCount': 10, 'Name': 'on-failure'},
                                                             }
                                              )

          # container = Docker::Container.create('Image' => DOCKER_IMG,
          #                                      'Volumes':
          #                                        {
          #                                          '/mnt/graphs' => {}
          #                                        },
          #                                      'HostConfig': {'PortBindings':
          #                                                       {
          #                                                         '9999/tcp':
          #                                                          [{ "HostPort": "#{endpoint[:port]}" }]
          #                                                       },
          #                                                     'Binds': ["#{current_dir}/#{@volume}/#{endpoint[:database]}:/mnt/graphs"],
          #                                                     'RestartPolicy': {'MaximumRetryCount': 10, 'Name': 'on-failure'},
          #                                                    }
          #                                     )


          container.start
          container.rename(endpoint[:database])
          endpoint[:docker_id] = container.info['id']
          puts "NEW #{endpoint[:database]} Container ID: #{container.info['id'][0..12]}"
          sleep 5               # breathe 5 seconds to let blazegraph (java) boot

        end

      end

    end

  end


  # will start a container with db name
  def start_db_container db, memory

    print_banner db
    edp = get_endpoint_from_db_name db
    start_container edp, memory

  end

  # will start all containers
  def start_all_containers

    @endpoints.each do |edp|
      start_container edp, "4g"
    end

    save_current_config

  end

  # bootstrap a container
  def bootstrap_container db

    if db.class == String
      edp = get_endpoint_from_db_name db
    elsif db.class == Hash
      edp = db
    else
      return
    end

    print_banner edp[:database]
    single_volume_files_checkup edp
    stop_container edp
    load_RDF_into_blazegraph edp
    start_container edp, "4g"

  end


  # will start all containers
  def bootstrap_all_containers

    puts "boostrap"
    @endpoints.each do |edp|
      bootstrap_container edp
    end

    save_current_config

  end



  # download graph data in FILE_FMT format (path need to be in config file :download_path)
  def download_graph_rawdata

    @endpoints.each do |dat|
      ShellSpinner "Downloading graph files for #{dat[:database]}" do
        Dir.mkdir("#{dat[:database]}")
        Dir.chdir("#{dat[:database]}")
        Dir.mkdir("raw_data")
        Dir.chdir("raw_data")
        filter_files = ""
        FILE_FMT.each do |fmt|
          filter_files += "-A '*.#{fmt}' "
        end
        `wget -q -e robots=off -r -nH --cut-dirs=3 --no-parent #{filter_files} #{dat[:download_path]} -R sitemap.xml`
      end
      files = Dir["./*"]
      puts "  Downloaded #{files.length()} files :"
      puts "    " + files.join("\n    ")
      Dir.chdir("../../")
    end

  end

  # will return endpoint from a db name
  def get_endpoint_from_db_name endpoint

    @endpoints.each do |edp|
      if edp[:database] == endpoint
        return edp
      end
    end

    return nil

  end


  # status of endpoints
  def print_status


  end

  # save current config
  def save_current_config

    date = Time.new.to_s.split(" ")[0]
    File.open("#{@config}.new.#{date}","w") do |f|
      f.write(@endpoints.to_yaml)
    end

  end


  # print db banner
  def print_banner database
    puts ""
    puts "##################################################"
    puts "#          #{database}"
    puts "##################################################"
  end


end


# # Main CLI # #

usage = "
 dockerize-endpoints.rb

    -c, --config <endpoints.yaml> 	see below the example of a yaml config file
    -v, --volume <volumes FULL_PATH> 	subdir are DB's name and each will be mounted into its corresponding DB's container

    OPTIONS:
       DATABASES
          --databases-info		info on available databases
          --download-db			database's URL has to be in the config file [*.nt.gz, *.nq.gz, *.ttl.gz, *.owl, *.ttl]

       DOCKER
          --docker-status		status of endpoint's container
          --docker-bootstrap <db>	setup and launch the endpoint's <upload db graph into it>
          --docker-bootstrap-all	setup and launch all endpoint containers <upload db graph into it>
          --docker-loader-ram <0-1>	fraction of RAM to use, default : 0.8 = 80% of avail RAM

          --docker-start <db>		launch the db endpoint container
          --docker-start-all		launch all endpoint containers

          --memory			java heap size for blazegraph [could be percentage total host ram 80% or 4g or 1024m]

       CONFIG
          --save-config			save the config to file

################################


See tests/configs/bio2rdf-metadata.yaml.test for an example of yaml config file.
Essentially it's an array of endpoint hash with the following keys :
  database, source, description, download_path, date, nb_of_triples, nb_of_entities

"


# Set the default options
def default_options

  options = {}
  options[:ram_ratio] = 0.8

  return options
end

# Parse the Options given on the CLI
def parse_options

  options = default_options

  while x = ARGV.shift

    case x.downcase
    when "-c", "--config"
      options[:config] = ARGV.shift
    when "-v", "--volume"
      options[:volume] = ARGV.shift
    when "--download-db"
      options[:download] = 1
    when "--docker-bootstrap"
      options[:bootstrap_db] = ARGV.shift
    when "--docker-bootstrap-all"
      options[:bootstrap_all] = 1
    when "--docker-loader-ram"
      options[:ram_ratio] = ARGV.shift.to_f
    when "--docker-start"
      options[:start_db] = ARGV.shift
    when "--docker-start-all"
      options[:start_all] = 1
    when "--memory"
      options[:memory] = ARGV.shift
    when "--save-config"
      options[:save_config] = 1
    when "--help", "-h"
      usage
    end

  end

  options

end

@options = parse_options

if ! @options.has_key? :config or ! @options.has_key? :volume
  abort usage
end

endpoints = EndpointDockerizer.new(@options[:config], @options[:volume], @options)
# endpoints.add_port_to_db

# Execute CLI commands
endpoints.download_graph_rawdata if @options[:download] == 1
endpoints.start_all_containers if @options[:start_all] == 1
endpoints.bootstrap_all_containers if @options[:bootstrap_all] == 1


if ! @options[:bootstrap_db].nil?
  endpoints.bootstrap_container @options[:bootstrap_db]
end

if ! @options[:start_db].nil?

  if ! @options[:memory].nil?
    endpoints.start_db_container @options[:start_db], @options[:memory]
  else
    endpoints.start_db_container @options[:start_db], "4g"
  end

end

if ! @options[:save_config].nil?
  endpoints.save_current_config
end
