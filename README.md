# Dockerized lifedata

The project aims to provide a CLI to deploy and manage docker's container for RDF databases based on a blazegraph triplestore engine.

see : https://hub.docker.com/r/zorino/docker-blazegraph/

The user needs to provide a configuration file (yaml) that contains informations on the endpoints.

The use case of this project is for Bio2RDF but could work with any collection of RDF endpoints with the right configuration.

### Installation

Install the dependencies for dockerized-endpoints.rb
```bash
bundle install
```

User must be in the docker's group or since docker needs surper-user privileges you need to launch it as such.
We also recommend using RVM as your ruby version manager, if it's not the case use sudo instead of rvm after having installed the gem dependencies.

```bash
rvmsudo ruby dockerized-endpoints -h
```


## Configuration

### Config file
Example of a configuration (yaml) file, check in configs/ :

```lang
---
- :database: affymetrix
  :full_name: Affymetrix probesets [affymetrix]
  :source: http://www.affymetrix.com/
  :description: This dataset contains the probesets used in the Affymetrix microarrays.
  :download_path: http://download.bio2rdf.org/release/3/affymetrix/
  :date: '2014-08-01'
  :nb_of_triples: '86942371'
  :nb_of_entities: '86942371'
- :database: biomodels
  :full_name: A Database of Annotated Published Models [biomodels]
  :source: http://www.ebi.ac.uk/biomodels/
  :description: BioModels Database is a data resource that allows biologists to store,
    search and retrieve published mathematical models of biological interests.
  :download_path: http://download.bio2rdf.org/release/3/biomodels/
  :date: '2014-06-05'
  :nb_of_triples: '2380009'
  :nb_of_entities: '2380009'
```
### Volumes
The volumes directory needs to contain subdirectory for each database supported, these volume will be mount respectively on their docker's container database.


